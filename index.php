<?php
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Name Test</title>
        <link rel="stylesheet" type="text/css" 
              href="main.css"/>
    </head>
    <body>
        <h1>Chapter 2 Selected Examples</h1>
        <h2>Fill in the form below if you want the results to display
           a name of your choosing, using the HTTP GET request</h2>
        <form action="display_get.php" method="get">
            <label>First name: </label>
            <input type="text" name="first_name"/><br>
            <label>Last name: </label>
            <input type="text" name="last_name"/><br>
            <label>&nbsp;</label>
            <input type="submit" value="Submit"/>
        </form>
        <h2>Or, you can click on the link below, which includes a GET request
           as part of its URL to display a "hard-coded" first name &amp; last name</h2>
        <p>
            <a href="display_get.php?first_name=Joel&last_name=Murach">
            I'm a link that performs a GET request</a>
        </p>
        <hr>
        <h2>Here's a form that uses the HTTP POST request</h2>
        <form action="display_post.php" method="post">
            <label>First name: </label>
            <input type="text" name="first_name"/><br>
            <label>Last name: </label>
            <input type="text" name="last_name"/><br>
            <label>&nbsp;</label>
            <input type="submit" value="Submit"/>
        </form>
        <h2>But with a POST request, there's no way to transmit data
            via the URL, even if you include the parameters as part of 
            the URL (as in a GET request). Try clicking on the link below...</h2>
        <p>
            <a href="display_post.php?first_name=Joel&last_name=Murach">
                This may not work the way you think (or hope) it does...
            </a>
        </p>
    </body>
</html>
